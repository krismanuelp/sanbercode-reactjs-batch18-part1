// soal 1

function munculkanKata()
{
    return "Halo Sanbers!"
}

var halo = munculkanKata();
console.log(halo);

// soal 2
var num1 = 12;
var num2 = 4;

function kalikan(num1, num2)
{
    return num1*num2
}

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

// soal 3
var name = "Krisma"
var age = 20
var address = "Jalan belum jadi"
var hobby = "Gaming"

function introduce(name, age, address, hobby)
{
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobi yaitu " + hobby
}

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)

// soal 4
var pesertaObj =
{
    nama: "Asep",
    "jenis kelamin": "Laki-laki",
    hobi: "baca buku",
    "tahun lahir": 1992
}
console.log(pesertaObj["jenis kelamin"])

// soal 5
var buah = 
[
    {
        nama: "strawberry",
        warna: "merah",
        "ada bijinya": "tidak",
        harga: 9000
    },
    {
        nama: "jeruk",
        warna: "oranye",
        "ada bijinya": "ada",
        harga: 8000
    },
    {
        nama: "Semangka",
        warna: "Hijau dan merah",
        "ada bijinya": "ada",
        harga: 10000
    },
    {
        nama: "Pisang",
        warna: "Kuning",
        "ada bijinya": "tidak",
        harga: 5000
    }
]

console.log(buah[0])

// soal 6
var dataFilm = []
function film(nama, durasi, genre, tahun)
{
    dataFilm.push({nama, durasi, genre, tahun})
}

film("Hacksaw Ridge", "2 jam lebih", "Perang", "2017")
film("Saving Private Ryan", "2 jam lebih juga", "Perang", "1998")
console.log(dataFilm)