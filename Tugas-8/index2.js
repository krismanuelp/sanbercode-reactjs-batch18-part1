var readBooksPromise = require('C:/Users/Asus/Documents/Front-End/promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let indeks = 0
let times = 10000

const amountOfBooksIRead = (time) =>
{
    if(books[indeks])
    {
        readBooksPromise(time, books[indeks])
            .then((result) =>
            {
                console.log(result + " result")
                time = result
                indeks++
                amountOfBooksIRead(time)
            })
            .catch((err) =>
            {
                console.log(err + " err")
            })
    }
    else
    {
        console.log("End")
    }
}

amountOfBooksIRead(times)