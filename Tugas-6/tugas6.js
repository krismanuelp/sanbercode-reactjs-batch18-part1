// soal 1

let r = 7
let phi = 22/7

const luasLingkaran = () =>
{
    let x = phi*r*r
    console.log(x)
}

luasLingkaran()

const kelilingLingkaran = () =>
{
    let y = 2*phi*r
    console.log(y)
}

kelilingLingkaran()

// soal 2
let kalimat = ""

const jawaban = function literal(satu, dua, tiga, empat, lima)
{
    satu
    dua
    tiga
    empat
    lima
    let kalimat = satu + " " + dua + " " + tiga + " " + empat + " " + lima
    console.log(kalimat)
}

jawaban("saya", "adalah", "seorang", "frontend", "developer")

// soal 3
const newFunction = function literal(firstName, lastName)
{
      firstName
      lastName
      let fullName = firstName + " " + lastName
      console.log(fullName)
}
   
//Driver Code 
 newFunction("William", "Imoh")

// soal 4
const newObject = 
{
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject

// Driver code
console.log(firstName, lastName, destination, occupation, spell)

// soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]

//Driver Code
console.log(combined)