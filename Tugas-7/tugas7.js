// soal 1

// Release 0

var legs = 4
var cold_blooded = false

class Animal
{
    constructor(name)
    {
        this._animalname = name
        this._animallegs = legs
        this._bloods = cold_blooded
    }
    get animalname()
    {
        return this._animalname
    }
    get animallegs()
    {
        return this._animallegs
    }
    get bloods()
    {
        return this._bloods
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.animalname) // "shaun"
console.log(sheep.animallegs) // 4
console.log(sheep.bloods) // false

// Release 1
var sound = "Auooo"

class Ape extends Animal
{
    constructor(name, _yell)
    {
        super(name)
        this._yell = sound
    }
    get yell()
    {
        return this._yell
    }
}

var lompat = "hop hop"

class Frog extends Animal
{
    constructor(name, _jump)
    {
        super(name)
        this._jump = lompat
    }
    get jump()
    {
        return this._jump
    }
}

var sungokong = new Ape("kera sakti")
console.log(sungokong.yell) // "Auooo"

var kodok = new Frog("buduk")
console.log(kodok.jump) // "hop hop"

// Soal 2

class Clock
{
    constructor({template})
    {
        this._template = template
        this.timer
    }
    render()
    {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this._template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }
    stop()
    {
        clearInterval(this.timer)
    }
    start()
    {
        this.render()
        this.timer = setInterval(() => this.render(), 1000)
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 