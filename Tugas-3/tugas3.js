// soal 1
var kataPertama = "saya ";
var kataKedua = "Senang ";
var kataKetiga = "belajar ";
var kataKeempat = "JAVASCRIPT";

console.log(kataPertama.concat(kataKedua, kataKetiga, kataKeempat));

// soal 2
var number1 = Number("1");
var number2 = Number("2");
var number3 = Number("4");
var number4 = Number("5");

console.log(number1+number2+number3+number4);

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// soal 4
var nilai = 0;

if(nilai >= 80)
{
    console.log("Indeks A");
}
else if(nilai >= 70 && nilai < 80)
{
    console.log("Indeks B");
}
else if(nilai >= 60 && nilai < 70)
{
    console.log("Indeks C");
}
else if(nilai >= 50 && nilai < 60)
{
    console.log("Indeks D");
}
else
{
    console.log("Indeks E");
}

// soal 5
var tanggal = "6 ";
var bulan = 12;
var tahun = "1999 ";

switch(bulan)
{
    case 1: {console.log(tanggal.concat('Januari ', tahun)); break;}
    case 2: {console.log(tanggal.concat('Februari ', tahun)); break;}
    case 3: {console.log(tanggal.concat('Maret ', tahun)); break;}
    case 4: {console.log(tanggal.concat('April ', tahun)); break;}
    case 5: {console.log(tanggal.concat('Mei ', tahun)); break;}
    case 6: {console.log(tanggal.concat('Juni ', tahun)); break;}
    case 7: {console.log(tanggal.concat('Juli ', tahun)); break;}
    case 8: {console.log(tanggal.concat('Agustus ', tahun)); break;}
    case 9: {console.log(tanggal.concat('September ', tahun)); break;}
    case 10: {console.log(tanggal.concat('Oktober ', tahun)); break;}
    case 11: {console.log(tanggal.concat('November ', tahun)); break;}
    case 12: {console.log(tanggal.concat('Desember ', tahun)); break;}
    default: {console.log('False');}
}