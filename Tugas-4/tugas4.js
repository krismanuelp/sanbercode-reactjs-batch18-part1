// soal 1
var angka = 2;
console.log("LOOPING PERTAMA");
while(angka <= 20)
{
    console.log(+ angka, "- I love coding");
    angka = angka + 2;
}

var number = 20;
console.log("LOOPING KEDUA");
while(number > 0)
{
    console.log(+ number, "- I will become a frontend developer");
    number = number - 2;
}

// soal 2
for(var num = 1; num <= 20; num++)
{
    if(num % 2 == 0)
    {
        console.log(+ num, "- Berkualitas")
    }
    else if(num % 3 == 0 && num % 2 != 0)
    {
        console.log(+ num, "- I Love Coding")
    }
    else
    {
        console.log(+ num, "- Santai")
    }
}

// soal 3
for(var i=1; i<=7; i++)
{
    console.log("#".repeat(i));
}

// soal 4
var kalimat = "saya sangat senang belajar javascript";
var x = kalimat.split(" ");
console.log(x);

// soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();

for(var i = 0; i < daftarBuah.length; i++)
{
    console.log(daftarBuah[i]);
}